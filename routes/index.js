const companyController = require('../controllers').company;
const employeeController = require('../controllers').employee;

module.exports = (app) =>{
    app.get('/api', (req,res) =>{
        res.status(200).send("API's working extremly fine");
    })

    app.get('/api/get_all_companies', companyController.findAll)

    app.get('/api/get_company/:id',companyController.findOne)

    app.post('/api/company', companyController.create)

    app.get('/api/company/employee/:id', companyController.findByPk)

    app.post('/api/employee/add', employeeController.create)


}