const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');

const http = require('http');

const app = express();

app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))

var models = require('./models') ;

models.sequelize.sync().then(() =>{
    console.log('Database connections works fine!!');
})
    .catch(err =>{
        console.log('something went wrong with database');
    })

require("./routes")(app);

const port = parseInt(process.env.PORT, 10) || 8000;
app.set('port', port);
const server = http.createServer(app);
server.listen(port);
module.exports = app;