const Employee = require('../models').Employee;

module.exports = {
    create(req, res){
        return Employee
            .create({
                name : req.body.name,
                designation: req.body.designation,
                salary: req.body.salary,
                companyId: req.body.companyId
            })
            .then(employee =>{
                res.status(201).send({"success": true, message: "Employee added successfully",employee})
            })
            .catch(err =>{
                res.status(400).send({"success": false, message: "Error in adding Employee",err})
            })
    }
}