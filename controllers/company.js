const Company = require('../models').Company;

module.exports = {
    create(req, res){
        return Company
            .create({
                name : req.body.name,
            })
            .then(company =>{
                res.status(201).send(company);
            })
            .catch(err=>{
                res.status(400).send(err);
            })

    },
    findAll(req, res){
        return Company
            .findAll({

            })
            .then(company =>{
                res.status(200).send(company);
            })
            .catch(err=>{
                res.status(400).send(err);
            })
    },
    findOne(req,res){
        return Company
            .findOne({
            where : {id : req.params.id}
            })
            .then(company =>{
                res.status(200).send(company);
            })
            .catch(err=>{
                res.status(400).send(err);
            })
    },
    findByPk(req,res){
        return Company
            .findByPk(req.params.id, {include : ['employees']})
            .then(company =>{
                res.status(200).send(company);
            })
            .catch(err =>{
                res.status(400).send(err);
            })
    }

}